#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#define MAX_THREADS 100
#define MAX_VALUE 9
#define MIN_VALUE 0

// struct para pasar los datos de calculo.
typedef struct
{
    int *row;
    int rowIndex;
    int scalar;
} Param;
// Define una matriz dinámica.
typedef struct
{
    int **dMatrix;
    int rows;
    int columns;
} Dmatrix;

// ---------- Headers ----------------- //
int askForData(Dmatrix *inputMatrix);
Dmatrix createMatrix(int rowsAmount, int columnsAmount);
void freeMatrixMemory(Dmatrix matrix);
void initializeRandomMatrix(Dmatrix matrix, int maxValue, int minValue);
void runMatrixByScalar(Dmatrix *inputMatrix, Param arg, int scalar);
void joinAllThreads(pthread_t *thread, int amountThreads);
int launchThread(pthread_t *thread, Param *argv);
void *multiplyRow(void *args);
int getRandomValue(int bottom, int top);
void printMatrix(Dmatrix matrix);
void swapInt(int *a, int *b);
void printFunctionName(const char funcName[], char *extraMssg);
Param copyParam(Param *source);
void PrintAthorDates(char *playerName);

Dmatrix resultMatrix;
pthread_attr_t attr;

int main()
{
    PrintAthorDates("Fsolaje");

    // Inicializamos los parametros de los hilos.
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

    // Inicializamos la semilla para la obtención de números aleatorios.
    srand(time(NULL));

    Dmatrix inputMatrix;
    inputMatrix.dMatrix = NULL;

    // Argumentos para pasar a los hilos.
    Param arguments;

    // Obtenemos los valores para la ejecución del programa.
    const int scalar = askForData(&inputMatrix);

    // Creación de la Matriz e inicialización.
    inputMatrix = createMatrix(inputMatrix.rows, inputMatrix.columns);
    initializeRandomMatrix(inputMatrix, MAX_VALUE, MIN_VALUE);
    // Imprimimos la matriz generada.
    printMatrix(inputMatrix);
    // Inicializamos la matriz resultado
    resultMatrix = createMatrix(inputMatrix.rows, inputMatrix.columns);

    runMatrixByScalar(&inputMatrix, arguments,scalar);

    //Imprimimos la Matriz resultado
    printf(" --- Multiplied Matrix ---");
    printMatrix(resultMatrix);

    pthread_attr_destroy(&attr);

    //Liberación de reservas de memoria
    freeMatrixMemory(inputMatrix);
    freeMatrixMemory(resultMatrix);

    return 0;
}

int askForData(Dmatrix *inputMatrix)
{
    int scalar;

    // Obtener las dismensiones de la matriz.
    printf(" --- Definición de Matriz ---\n");

    printf("Introduce el número de Filas: ");
    scanf("%d", &inputMatrix->rows);
    // Limita el número de filas al número de hilos.
    inputMatrix->rows = inputMatrix->rows > MAX_THREADS ? 100 : inputMatrix->rows;

    printf("Introduce el número de Columnas: ");
    scanf("%d", &inputMatrix->columns);

    printf("Introduce el escalar: ");
    scanf("%d", &scalar);

    if (inputMatrix->rows == 0 || inputMatrix->columns == 0)
    {
        inputMatrix->rows = 0;
        inputMatrix->columns = 0;
    }

    return scalar;
}

Dmatrix createMatrix(int rowsAmount, int columnsAmount)
{
    Dmatrix newMatrix;
    newMatrix.dMatrix = (int **)malloc(rowsAmount * sizeof(int *));
    newMatrix.columns = columnsAmount;
    newMatrix.rows = rowsAmount;

    for (int i = 0; i < rowsAmount; i++)
    {
        newMatrix.dMatrix[i] = (int *)malloc(columnsAmount * sizeof(int));
    }

    return newMatrix;
}

void runMatrixByScalar(Dmatrix *inputMatrix, Param arguments, const int scalar)
{
    // Definición de los Hilos.
    pthread_t *thread = (pthread_t *)malloc(sizeof(pthread_t) * inputMatrix->rows);

    // Parametros que se le pasára a los hilos
    // Es importante pasar los argumentos independientes a cada uno de los hilos
    // para que no se generen conflictos. Ya que hay una de las propiedades de los Param
    // que varía en cada vuelta de bucle.
    Param *tempArg = (Param *)malloc(sizeof(Param) * inputMatrix->rows);

    for (int rowIndex = 0; rowIndex < inputMatrix->rows; rowIndex++)
    {
        tempArg[rowIndex].row = inputMatrix->dMatrix[rowIndex];
        tempArg[rowIndex].rowIndex = rowIndex;
        tempArg[rowIndex].scalar = scalar;

        if (0 != launchThread(&thread[rowIndex], &tempArg[rowIndex]))
        {
            printf("\n###### Error In Threads -------------");
            exit(1);
        }
    }

    joinAllThreads(thread, inputMatrix->rows);

    free(thread);
    free(tempArg);
}
// Unimos todos los hilos que se hayan creado con el hilo(proceso) principal.
void joinAllThreads(pthread_t *thread, int amountThreads)
{
    for (int i = 0; i < amountThreads; i++)
    {
        pthread_join(thread[i], NULL);
    }
}

// Libera la memoria de una matriz.
void freeMatrixMemory(Dmatrix matrix)
{
    printFunctionName(__func__, NULL);

    for (int i = 0; i < matrix.rows; i++)
    {
        free(matrix.dMatrix[i]);
    }
    free(matrix.dMatrix);
}

// Inicializa a valores 0 cada una de las celdas de la matriz
void initializeRandomMatrix(Dmatrix matrix, int maxValue, int minValue)
{
    printFunctionName(__func__, NULL);

    for (int x = 0; x < matrix.rows; x++)
    {
        for (int y = 0; y < matrix.columns; y++)
        {
            matrix.dMatrix[x][y] = getRandomValue(maxValue, minValue);
        }
    }
}

// Devuelve un entero aleatorio en un rango dado.
int getRandomValue(int bottom, int top)
{
    if (bottom > top)
    {
        swapInt(&bottom, &top);
    }
    return rand() % (top - bottom) + bottom;
}

// Intercambia valores de variables int.
void swapInt(int *a, int *b)
{
    int aux = *a;
    *a = *b;
    *b = aux;
}

void printMatrix(Dmatrix matrix)
{
    printFunctionName(__func__, NULL);
    printf("\n");
    for (int i = 0; i < matrix.rows; i++)
    {
        for (int j = 0; j < matrix.columns; j++)
        {
            printf("%5d", matrix.dMatrix[i][j]);
        }
        printf("\n\n");
    }
}

int launchThread(pthread_t *thread, Param *argv)
{

    int status = pthread_create(thread, &attr, multiplyRow, (void *)argv);
    // Crea un hilo con los argumentos pasados.
    return status;
}

void *multiplyRow(void *arg)
{
    Param *arguments = arg;

    int rowIndex = arguments->rowIndex;
    int scalar = arguments->scalar;
    int *row = arguments->row;

    //Calculating Each Element in ResulresultMatrixix Using Passed Arguments
    for (int i = 0; i < resultMatrix.columns; i++)
    {
        resultMatrix.dMatrix[rowIndex][i] = row[i] * scalar;
    }

    // Fin del hilo
    pthread_exit(0);
}

void printFunctionName(const char funcName[], char *extraMssg)
{
    extraMssg = extraMssg == NULL ? "" : extraMssg;

    printf("\n### %s %s", funcName, extraMssg);
}

Param copyParam(Param *source)
{
    Param copy;
    copy.row = source->row;
    copy.rowIndex = source->rowIndex;
    copy.scalar = source->scalar;

    return copy;
}

void PrintAthorDates(char *playerName)
{
    printf("\n#-------- Multiplicación de Matriz --------#\n");
    printf("#             por un escalar               #\n");
    printf("        By %s ", playerName);
    printf("\n#-------------------------------------------#\n\n");

    return;
}
